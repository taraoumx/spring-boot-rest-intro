# spring-boot-rest-intro

This is a spring boot project meant to introduce  **RESTful Web Services** done the spring way. 

# Pre-Requisites 

- [ ] JDK 8 installed
- [ ] Maven 
- [ ] IDE ( Eclipse or IntelliJ) - preferrably, **IntelliJ**

# Instructions 

1. Create a feature branch based off of the master branch, call this branch  
> **feature-spring-boot-rest**
2. Head over to the spring [starter project](https://start.spring.io/)  and create a project by selecting the following : 
    - Project : **Maven**
    - Language : **Java**
    - Spring Boot: **2.3.1**
    - Dependencies : Click add dependencies and select **spring-web** from the list to add. 
    
    Leave all other project metadata at default values. 

3. Create a class called **RestController** and create a restful method in it with the following criterial :
    - name : **helloWorld**
    - mapping type : **GET**
    - params : **none** 
    - return type : **String** 
    - return value : **"Hello World"**
    

For a working example please refer to this tutorial : https://spring.io/guides/gs/rest-service/ 

4. Once it is working, push the changes to the repo under that branch **feature-spring-boot-rest**
5. Create a pull request to merge changes into master branch. 


    